package ru.tynixcloud.sdk;

/**
 * Created by kaytato, for tynixcloud.
 * @VK: https://vk.com/kaytato
 */
public class ExampleClass {

    public static void main(String[] args) throws Exception {
        TynixSdk sdk = new TynixSdk("you-token");
        sdk.setDebug(true);
        System.out.println(String.format("[TynixSDK] :: Group player Frotter_ = %s",
                sdk.PLAYER_GROUP_REQUEST.getPlayerGroupResponse("Frotter_").getGroupId()));
        System.out.println(String.format("[TynixSDK] :: Locale player Frotter_ = %s",
                sdk.PLAYER_INFORMATION_REQUEST.getPlayerInformationResponse("Frotter_").getLocale()));
        System.out.println(String.format("[TynixSDK] :: Player with id 2 = %s",
                sdk.PLAYER_NAME_REQUEST.getPlayerNameResponse(2).getName()));
        System.out.println(String.format("[TynixSDK] :: Online mc.tynixcloud.ru = %s",
                sdk.GLOBAL_ONLINE_REQUEST.getTotalOnlineResponse()));

        System.out.println(String.format("[TynixSDK] :: Prefix of group with level 100 = %s",
                sdk.GROUPS_REQUEST.getGroupResponse(100).getPrefix()));
        System.out.println(String.format("[TynixSDK] :: Guild player Frotter_ = %s",
                sdk.PLAYER_GUILD_REQUEST.getPlayerGuildResponse("Frotter_").getGuildName()));

    }

}
