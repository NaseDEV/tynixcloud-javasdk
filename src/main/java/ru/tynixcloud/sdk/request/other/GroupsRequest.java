package ru.tynixcloud.sdk.request.other;

import com.google.gson.JsonObject;
import lombok.*;
import lombok.experimental.FieldDefaults;
import ru.tynixcloud.sdk.TynixSdk;
import ru.tynixcloud.sdk.request.BaseRequest;
import ru.tynixcloud.sdk.utility.HttpParam;
import ru.tynixcloud.sdk.utility.JsonUtil;

import java.io.IOException;
import java.util.concurrent.ExecutionException;
/**
 * Created by kaytato, for tynixcloud.
 * @VK: https://vk.com/kaytato
 */
public class GroupsRequest extends BaseRequest {
    public GroupsRequest(@NonNull TynixSdk sdk) {
        super(sdk, "other/groups");
    }

    public GroupResponse getGroupResponse(int groupId) throws ExecutionException, InterruptedException, IOException {
        JsonObject response = JsonUtil.parse(get(request, new HttpParam("id", "" + groupId)))
                .getAsJsonObject()

                .get("response")
                .getAsJsonArray().get(0)
                .getAsJsonObject();
        int id = response.get("id").getAsInt();
        String name = response.get("name").getAsString();
        int level = response.get("level").getAsInt();
        String prefix = response.get("prefix").getAsString();
        String suffix = response.get("suffix").getAsString();

        return new GroupResponse(id, name, level, prefix, suffix);
    }


    @Getter
    @RequiredArgsConstructor
    @AllArgsConstructor
    @FieldDefaults(level = AccessLevel.PRIVATE)
    public static class GroupResponse {

        int id;
        String name;
        int level;
        String prefix;
        String suffix;


    }
}
