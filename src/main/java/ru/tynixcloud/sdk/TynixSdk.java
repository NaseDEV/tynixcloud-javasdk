package ru.tynixcloud.sdk;

import lombok.*;
import ru.tynixcloud.sdk.exception.MethodNotFoundException;
import ru.tynixcloud.sdk.exception.ParamNotFoundException;
import ru.tynixcloud.sdk.exception.TokenHasExpiredException;
import ru.tynixcloud.sdk.request.other.OnlineRequest;
import ru.tynixcloud.sdk.request.other.GroupsRequest;
import ru.tynixcloud.sdk.request.player.PlayerGroupRequest;
import ru.tynixcloud.sdk.request.player.PlayerGuildRequest;
import ru.tynixcloud.sdk.request.player.PlayerInformationRequest;
import ru.tynixcloud.sdk.request.player.PlayerNameRequest;
import ru.tynixcloud.sdk.utility.HttpParam;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.concurrent.ExecutionException;

/**
 * Created by kaytato, for tynixcloud.
 * @VK: https://vk.com/kaytato
 */
@Getter
@RequiredArgsConstructor
@AllArgsConstructor
public class TynixSdk {


    private final String token;
    @Setter
    boolean debug = false;

    // ==========================================================================================================================================================
    public final PlayerGroupRequest PLAYER_GROUP_REQUEST = new PlayerGroupRequest(this);
    public final PlayerInformationRequest PLAYER_INFORMATION_REQUEST = new PlayerInformationRequest(this);
    public final PlayerNameRequest PLAYER_NAME_REQUEST = new PlayerNameRequest(this);
    public final OnlineRequest GLOBAL_ONLINE_REQUEST = new OnlineRequest(this);
    public final GroupsRequest GROUPS_REQUEST = new GroupsRequest(this);
    public final PlayerGuildRequest PLAYER_GUILD_REQUEST = new PlayerGuildRequest(this);
    // ==========================================================================================================================================================

    public String get(@NonNull String request, HttpParam... params) throws IOException, MethodNotFoundException,
            TokenHasExpiredException, ParamNotFoundException {
        String url = "http://api.tynixcloud.ru:8090/" + request + "?token=" + token;
        if (params != null)
            for (HttpParam param : params) {
                url+="&" + param.getKey() + "=" + param.getValue();
            }
        if (debug) System.out.println("[TynixSDK] :: Sending a GET request (" + url + ")..");
        URL obj = new URL(url);
        HttpURLConnection connection = (HttpURLConnection) obj.openConnection();

        connection.setRequestMethod("GET");

        BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
        String inputLine;
        StringBuffer response = new StringBuffer();

        while ((inputLine = in.readLine()) != null) {
            response.append(inputLine);
        }
        in.close();

        if (debug) System.out.println(String.format("[TynixSDK] :: Server has return %s", response.toString()));

        return response.toString();
    }

    /**
     * Получить глобальный онлайн.
     */
    public int getTotalOnline() throws IOException, ExecutionException, InterruptedException {
        return GLOBAL_ONLINE_REQUEST.getTotalOnlineResponse();
    }

    public PlayerInformationRequest.GetPlayerInformationResponse getPlayer(String nickname) throws IOException, ExecutionException, InterruptedException {
        return PLAYER_INFORMATION_REQUEST.getPlayerInformationResponse(nickname);
    }
}

